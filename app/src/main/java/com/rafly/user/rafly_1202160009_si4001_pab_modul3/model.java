package com.rafly.user.rafly_1202160009_si4001_pab_modul3;

public class model {
    private String nama, gawean, Kelamin;

    public model(String nama, String pekerjaan, String jenisKelamin) {
        this.nama = nama;
        this.gawean = pekerjaan;
        this.Kelamin = jenisKelamin;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPekerjaan() {
        return gawean;
    }

    public void setPekerjaan(String pekerjaan) {
        this.gawean = pekerjaan;
    }

    public String getJenisKelamin() {
        return Kelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.Kelamin = jenisKelamin;
    }
}
