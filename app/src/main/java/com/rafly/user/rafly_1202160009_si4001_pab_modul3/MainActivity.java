package com.rafly.user.rafly_1202160009_si4001_pab_modul3;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView rec;
    ArrayList<model> data = new ArrayList<>();
    adapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rec = findViewById(R.id.data);
        rec.setHasFixedSize(true);

        adapter = new adapter(data, new adapter.onclick() {
            @Override
            public void diklik(model saatIni) {
                Intent blabla = new Intent(MainActivity.this, lainnya.class);
                blabla.putExtra("nama", saatIni.getNama());
                blabla.putExtra("job", saatIni.getPekerjaan());
                blabla.putExtra("jeniskelamin", saatIni.getJenisKelamin());
                startActivity(blabla);
            }
        });

        rec.setLayoutManager(new LinearLayoutManager(this));
        rec.setAdapter(adapter);
        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                data.remove(viewHolder.getAdapterPosition());
                adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });
        helper.attachToRecyclerView(rec);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation==Configuration.ORIENTATION_LANDSCAPE){
            rec.setLayoutManager(new GridLayoutManager(this, 2));
        }else if (newConfig.orientation==Configuration.ORIENTATION_PORTRAIT){
            rec.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    public void tambahdata(View view) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
        final View dialogView = getLayoutInflater().inflate(R.layout.userbaru, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.setTitle("Create new user");
        final AlertDialog dlg = dialog.create();

        (dialogView.findViewById(R.id.submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(MainActivity.class.getSimpleName(), ((Spinner)dialogView.findViewById(R.id.jeniskelamin)).getSelectedItem().toString());
                adapter.addItem(new model(
                        ((EditText)dialogView.findViewById(R.id.baru)).getText().toString(),
                        ((EditText)dialogView.findViewById(R.id.gawe)).getText().toString(),
                        ((Spinner)dialogView.findViewById(R.id.jeniskelamin)).getSelectedItem().toString()

                ));

                dlg.dismiss();
            }
        });

        (dialogView.findViewById(R.id.cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { dlg.dismiss(); }
        });

        dlg.show();
    }
}
